# Copyright 2021 cnngimenez

# Author: cnngimenez

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

function download_ics
    set -l passwd ''
    set -l username ''
    set -l url ''

    read -s passwd
    curl -X PROPFIND -u "$username:$passwd"  -H 'Content-type: text/xml' --data '<propfind xmlns=\'DAV:\'><prop><calendar-data xmlns=\'urn:ietf:params:xml:ns:caldav\'/></prop></propfind>' "$url" > output.xml
    gawk -f take-ical.awk output.xml > output.ics
end

function process_ical2org
    set -l output_file 'output.org'

    echo "#+TITLE: Birthdays\n" > "$output_file"

    for f in *.ics
        echo "Processing $f"
        ruby ical2org/ical2org.rb < "$f" >> "$output_file"
    end
end

download_ics
process_ical2org
